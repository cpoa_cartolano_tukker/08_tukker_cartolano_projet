package MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import dao.Connexion;
import dao.InterfaceGenericProduitDao;
import metiers.Produit;


public class MySQLProduitDao implements InterfaceGenericProduitDao {

	private static MySQLProduitDao instance;

	public static MySQLProduitDao getInstance() {

		if (instance == null) {
			instance = new MySQLProduitDao();
		}

		return instance;
	}

	@Override
	public Produit getById(int id) {
		Produit produit=null;
		try {
			PreparedStatement requete = Connexion.creerConnexion().prepareStatement("SELECT * FROM produit WHERE ref=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			if (res.next()) {
				produit = new Produit(res.getInt("ref"),
						res.getString("nom"),
						res.getDouble("prixu"),
						res.getInt("TypeProduit")); 
					
				res.close();
			} else {
				//System.out.println("Produit inexistant !");
			}

			if (requete != null) {
				requete.close();
			}

		} catch (SQLException se) {
			System.out.println("Pb SQL " + se.getMessage());
		}

		return produit;
	}

	@Override
	public int create(Produit objet) {
		int num=-1;
		try {
			PreparedStatement requete = Connexion.creerConnexion().prepareStatement(
					"INSERT INTO produit (nom, prixu, TypeProduit) VALUES (?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			requete.setString(1, objet.getNom());
			requete.setDouble(2, objet.getPrixu());
			requete.setInt(3, objet.getTypeProduit());
			requete.executeUpdate();

			ResultSet res = requete.getGeneratedKeys();
			if (res.next()) {
				
				num=res.getInt(1);
				res.close();
			}

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}

return num;
	}
	
	@Override
	public void update(Produit objet) {
		
		try {
		PreparedStatement requete = Connexion.creerConnexion().prepareStatement(
				"UPDATE produit SET nom=?, prixu=?, TypeProduit=? WHERE ref=?");
		requete.setString(1, objet.getNom());
		requete.setDouble(2, objet.getPrixu());
		requete.setInt(3,objet.getTypeProduit());
		requete.setInt(4, objet.getRef());
		requete.executeUpdate();

		if (requete != null) {
			requete.close();
		}
	} catch (SQLException se) {
		System.err.println("Pb SQL :" + se.getMessage());}
	}

	

	@Override
	public void delete(Produit objet) {
		try {
			PreparedStatement requete = Connexion.creerConnexion().prepareStatement("DELETE FROM produit WHERE ref=?");
			requete.setInt(1, objet.getRef());
			requete.executeUpdate();

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}
	}

	public String AfficherRef(){
		return this.AfficherRef();
	}

	@Override
	public List<Produit> findAllProduit() {
		List<Produit> listprod = new ArrayList<Produit>();
		try{
		PreparedStatement requete = Connexion.creerConnexion().prepareStatement("SELECT * FROM Produit ");
		
		ResultSet res = requete.executeQuery();

		while (res.next()) {
			
			
			listprod.add(new Produit(res.getInt("ref"),
					res.getString("nom"),
					res.getDouble("prixu"),
					res.getInt("TypeProduit")) )  ;
				
			
			
		} 
		
		
		

		if (requete != null) {
			requete.close();
		}

	} catch (SQLException se) {
		System.out.println("Pb SQL " + se.getMessage());
	}

	return listprod;
	}

	


}
