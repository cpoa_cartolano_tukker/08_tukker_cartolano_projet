package MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import dao.Connexion;
import dao.InterfaceGenericTypeProduitDao;
import metiers.TypeProduit;


public class MySQLTypeProduitDao implements InterfaceGenericTypeProduitDao {

	private static MySQLTypeProduitDao instance;

	public static MySQLTypeProduitDao getInstance() {

		if (instance == null) {
			instance = new MySQLTypeProduitDao();
		}

		return instance;
	}

	@Override
	public TypeProduit getById(int id) {
		TypeProduit produit=null;
		try {
			PreparedStatement requete = Connexion.creerConnexion().prepareStatement("SELECT * FROM TypeProduit WHERE idtype=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			if (res.next()) {
				produit = new TypeProduit(res.getInt("idtype"),res.getString("type")); 
					
				res.close();
			} else {
				//System.out.println("TypeProduit inexistante !");
			}

			if (requete != null) {
				requete.close();
			}

		} catch (SQLException se) {
			System.out.println("Pb SQL " + se.getMessage());
		}

		return produit;
	}

	@Override
	public int  create(TypeProduit objet) {
		
		int num = -1;
		
		try {
			PreparedStatement requete = Connexion.creerConnexion().prepareStatement(
					"INSERT INTO TypeProduit (idtype, type) VALUES (?,?)",
					Statement.RETURN_GENERATED_KEYS);
			requete.setInt(1, objet.getIdtype());
			requete.setString(2, objet.getType());
			
			requete.executeUpdate();

			ResultSet res = requete.getGeneratedKeys();
			if (res.next()) {
				num=res.getInt(1);
				res.close();
			}

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}


		return num;
		
	}

	@Override
	public void update(TypeProduit objet) {
		
		try {
		PreparedStatement requete = Connexion.creerConnexion().prepareStatement(
				"UPDATE TypeProduit SET type=? WHERE idtype=?");
		requete.setString(1, objet.getType());
		requete.setInt(2, objet.getIdtype());
		requete.executeUpdate();

		if (requete != null) {
			requete.close();
		}
	} catch (SQLException se) {
		System.err.println("Pb SQL :" + se.getMessage());}
	}

	

	@Override
	public void delete(TypeProduit objet) {
		try {
			PreparedStatement requete = Connexion.creerConnexion().prepareStatement("DELETE FROM TypeProduit WHERE idtype=?");
			requete.setInt(1, objet.getIdtype());
			requete.executeUpdate();

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}
	}

	

	@Override
	public List<TypeProduit> findAllTypeProduit() {
		List<TypeProduit> listtp = new ArrayList<TypeProduit>();
		try{
		PreparedStatement requete = Connexion.creerConnexion().prepareStatement("SELECT * FROM TypeProduit ");
		
		ResultSet res = requete.executeQuery();

		while (res.next()) {
			
			
			listtp.add(new TypeProduit(res.getInt("idtype"),res.getString("type")));
				
			
			
		} 
		
		
		

		if (requete != null) {
			requete.close();
		}

	} catch (SQLException se) {
		System.out.println("Pb SQL " + se.getMessage());
	}

	return listtp;
	}

}	


