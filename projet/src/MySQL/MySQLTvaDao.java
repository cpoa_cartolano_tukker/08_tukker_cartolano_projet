package MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import dao.Connexion;
import dao.InterfaceGenericTvaDao;
import metiers.Tva;


public class MySQLTvaDao implements InterfaceGenericTvaDao {

	private static MySQLTvaDao instance;

	public static MySQLTvaDao getInstance() {

		if (instance == null) {
			instance = new MySQLTvaDao();
		}

		return instance;
	}

	@Override
	public Tva getById(int id) {
		Tva produit=null;
		try {
			PreparedStatement requete = Connexion.creerConnexion().prepareStatement("SELECT * FROM tva WHERE id=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			if (res.next()) {
				produit = new Tva(res.getInt("id"),res.getString("lib"),res.getDouble("taux")); 
					
				res.close();
			} else {
				//System.out.println("Tva inexistante !");
			}

			if (requete != null) {
				requete.close();
			}

		} catch (SQLException se) {
			System.out.println("Pb SQL " + se.getMessage());
		}

		return produit;
	}

	@Override
	public int create(Tva objet) {
		
		int num = -1;
		
		try {
			PreparedStatement requete = Connexion.creerConnexion().prepareStatement(
					"INSERT INTO tva (lib, taux) VALUES (?,?)",
					Statement.RETURN_GENERATED_KEYS);
			requete.setString(1, objet.getLib());
			requete.setDouble(2, objet.getTaux());
			
			requete.executeUpdate();

			ResultSet res = requete.getGeneratedKeys();
			if (res.next()) {
				num =res.getInt(1);
				res.close();
			}

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}

		
		return num;
		
	}

	@Override
	public void update(Tva objet) {
		
		try {
		PreparedStatement requete = Connexion.creerConnexion().prepareStatement(
				"UPDATE tva SET lib =?, taux=? WHERE id=?");
		requete.setString(1, objet.getLib());
		requete.setDouble(2, objet.getTaux());
		requete.setInt(3,  objet.getIdTva());
		requete.executeUpdate();

		if (requete != null) {
			requete.close();
		}
	} catch (SQLException se) {
		System.err.println("Pb SQL :" + se.getMessage());}
	}

	

	@Override
	public void delete(Tva objet) {
		try {
			PreparedStatement requete = Connexion.creerConnexion().prepareStatement("DELETE FROM tva WHERE id=?");
			requete.setInt(1, objet.getIdTva());
			requete.executeUpdate();

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}
	}

	

	@Override
	public List<Tva> findAllTva() {
		List<Tva> listtva = new ArrayList<Tva>();
		try{
		PreparedStatement requete = Connexion.creerConnexion().prepareStatement("SELECT * FROM tva ");
		
		ResultSet res = requete.executeQuery();

		while (res.next()) {
			
			
			listtva.add(new Tva(res.getInt("id"),res.getString("lib"),res.getDouble("taux")));
				
			
			
		} 
		
		
		

		if (requete != null) {
			requete.close();
		}

	} catch (SQLException se) {
		System.out.println("Pb SQL " + se.getMessage());
	}

	return listtva;
	}

	


}

