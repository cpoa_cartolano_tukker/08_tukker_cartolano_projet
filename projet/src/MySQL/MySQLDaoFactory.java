package MySQL;

import dao.DAOFactory;
import dao.InterfaceGenericClientDao;
import dao.InterfaceGenericFactureDao;
import dao.InterfaceGenericProduitDao;
import dao.InterfaceGenericTvaDao;
import dao.InterfaceGenericTypeProduitDao;

public class MySQLDaoFactory extends DAOFactory {

	@Override
	public InterfaceGenericTvaDao getTVADAO() {
		// TODO Auto-generated method stub
		return MySQLTvaDao.getInstance();
	}
	
	public InterfaceGenericTypeProduitDao getTypeProduitDAO() {
		// TODO Auto-generated method stub
		return MySQLTypeProduitDao.getInstance();
	}
	
	public InterfaceGenericProduitDao getProduitDAO() {
		// TODO Auto-generated method stub
		return MySQLProduitDao.getInstance();
	}
	
	public InterfaceGenericFactureDao getFactureDAO() {
		// TODO Auto-generated method stub
		return MySQLFactureDao.getInstance();
	}
	
	public InterfaceGenericClientDao getClientDAO() {
		// TODO Auto-generated method stub
		return MySQLClientDao.getInstance();
	}


	
}
