package testsMetiers;

import static org.junit.Assert.*;
import metiers.Produit;

import org.junit.Test;

public class TestProduit {

	
	// Tests Constructeur
	
	@Test
	public void testConstructErreur() {
		
		
		try{
		Produit p1 = new Produit(-1,"ballon",1.50,8);
		fail("erreur la reference est inferieur a 0");
		}
		catch(IllegalArgumentException e){};
	}
	
	
	@Test
	public void testConstructReussi() {
		
		
		try{
			Produit p1 = new Produit(1,"ballon",1.50,8);
		
		}
		catch(IllegalArgumentException e){
			fail("erreur la ref est inferieur ou egale a 0");};
		}
	
	
	
	// Test get et set ref
	
	@Test
	public void testGetRef(){
		Produit p1 = new Produit(1,"ballon",1.50,8);
		assertEquals(1,p1.getRef());
	}
	
	@Test
	public void testSetRefErreur(){
		try{
			Produit p1 = new Produit(1,"ballon",1.50,8);
			p1.setRef(-1);
			fail("erreur la ref est inferieur ou egale a 0");
		} catch(IllegalArgumentException e){}
	}
	
	@Test
	public void testSetRefReussi(){
		try{
			Produit p1 = new Produit(1,"ballon",1.50,8);
			p1.setRef(12);
			
		} catch(IllegalArgumentException e){
			fail("erreur la ref est inferieur ou egale a 0");}
	}
	
	
	// Test get et set Nom
	
	@Test
	public void testGetNom(){
		Produit p1 = new Produit(1,"ballon",1.50,8);
		assertEquals("ballon",p1.getNom());
	}
	
	@Test
	public void testSetNomErreur(){
		try{
			Produit p1 = new Produit(1,"ballon",1.50,8);
			p1.setNom(null);
			fail("erreur nom null");
		} catch(IllegalArgumentException e){}
	}
	
	@Test
	public void testSetNomReussi(){
		try{
			Produit p1 = new Produit(1,"ballon",1.50,8);
			p1.setNom("casquette");
			
		} catch(IllegalArgumentException e){
			fail("erreur nom null");}
	}
	
	// Test get et set prixu
	
		@Test
		public void testGetPrixu(){
			Produit p1 = new Produit(1,"ballon",1.50,8);
			assertEquals(1.50,p1.getPrixu(),0.0d);
		}
		
		@Test
		public void testSetprixuErreur(){
			try{
				Produit p1 = new Produit(1,"ballon",1.50,8);
				p1.setPrixu(-1);
				fail("erreur  le prix unitaire est inferieur ou egal a 0");
			} catch(IllegalArgumentException e){}
		}
		
		@Test
		public void testSetPrixuReussi(){
			try{
				Produit p1 = new Produit(1,"ballon",1.50,8);
				p1.setPrixu(200);
				
			} catch(IllegalArgumentException e){
				fail("erreur le prix unitaire est inferieur ou egal a 0");}
		}
		
		// Test get et set type produit
		
		@Test
		public void testGetTypeProduit(){
			Produit p1 = new Produit(1,"ballon",1.50,8);
			assertEquals(8,p1.getTypeProduit());
		}
		
		@Test
		public void testSetTypeProduitErreur(){
			try{
				Produit p1 = new Produit(1,"ballon",1.50,8);
				p1.setTypeProduit(-1);
				fail("erreur le type produit est inferieur ou egal a 0");
			} catch(IllegalArgumentException e){}
		}
		
		@Test
		public void testSetTypeProduitReussi(){
			try{
				Produit p1 = new Produit(1,"ballon",1.50,8);
				p1.setTypeProduit(121);
				
			} catch(IllegalArgumentException e){
				fail("erreur le type produit est inferieur ou egal a 0");}
		}
}
	