package testsMetiers;

import static org.junit.Assert.*;
import metiers.TypeProduit;

import org.junit.Test;

public class TestTypeProduit {

	
	// Tests Constructeur
	
	@Test
	public void testConstructErreur() {
		
		
		try{
		TypeProduit tp1 = new TypeProduit(-1,"chaise");
		fail("erreur l'id est inferieur ou egal a 0");
		}
		catch(IllegalArgumentException e){};
	}
	
	
	@Test
	public void testConstructReussi() {
		
		
		try{
			TypeProduit tp1 = new TypeProduit(1,"chaise");
		
		}
		catch(IllegalArgumentException e){fail("erreur l'id est inferieur ou egal a 0");};
	}
	
	
	// Test get et set idtype
	
	@Test
	public void testGetIdtype(){
		TypeProduit tp1 = new TypeProduit(1,"chaise");
		assertEquals(1,tp1.getIdtype());
	}
	
	@Test
	public void testSetIdtypeErreur(){
		try{
			TypeProduit tp1 = new TypeProduit(1,"chaise");
			tp1.setIdtype(-1);
			fail("erreur l'id est inférieur ou egal à 0");
		} catch(IllegalArgumentException e){}
	}
	
	@Test
	public void testSetIdtypeReussi(){
		try{
			TypeProduit tp1 = new TypeProduit(1,"chaise");
			tp1.setIdtype(3);
			
		} catch(IllegalArgumentException e){
			fail("erreur l'id est inferieur ou egal a 0");}
	}
	
	
	// Test get et set type
	
	@Test
	public void testGetType(){
		TypeProduit tp1 = new TypeProduit(1,"chaise");
		assertEquals("chaise",tp1.getType());
	}
	
	@Test
	public void testSetTypeErreur(){
		try{
			TypeProduit tp1 = new TypeProduit(1,"chaise");
			tp1.setType(null);
			fail("erreur type null");
		} catch(IllegalArgumentException e){}
	}
	
	@Test
	public void testSetTypeReussi(){
		try{
			TypeProduit tp1 = new TypeProduit(1,"chaise");
			tp1.setType("table");
			
		} catch(IllegalArgumentException e){
			fail("erreur type null");}
	
	}
}
	