package testsMetiers;

import static org.junit.Assert.*;
import metiers.Client;

import org.junit.Test;

public class TestClient {

	
	// Tests Constructeur
	
	@Test
	public void testConstructErreur() {
		
		
		try{
		Client client1 = new Client(-1,"jean","rue de la mairie");
		fail("erreur l'id est inferieur a 0");
		}
		catch(IllegalArgumentException e){};
	}
	
	
	@Test
	public void testConstructReussi() {
		
		
		try{
		Client client1 = new Client(1,"jean","rue de la mairie");
		
		}
		catch(IllegalArgumentException e){fail("erreur l'id est inferieur a 0");};
	}
	
	
	// Test get et set NomClient
	
	@Test
	public void testGetNomclient(){
		Client client1 = new Client(1,"jean","rue de la mairie");
		assertEquals("jean",client1.getNomclient());
	}
	
	@Test
	public void testSetNomclientErreur(){
		try{
			Client client1 = new Client(1,"jean","rue de la mairie");
			client1.setNomclient(null);
			fail("erreur nomclient null");
		} catch(IllegalArgumentException e){}
	}
	
	@Test
	public void testSetNomclientReussi(){
		try{
			Client client1 = new Client(1,"jean","rue de la mairie");
			client1.setNomclient("thomas");
			
		} catch(IllegalArgumentException e){
			fail("erreur nomclient null");}
	}
	
	
	// Test get et set Adclient
	
	@Test
	public void testGetAdclient(){
		Client client1 = new Client(1,"jean","rue de la mairie");
		assertEquals("rue de la mairie",client1.getAdclient());
	}
	
	@Test
	public void testSetAdclientErreur(){
		try{
			Client client1 = new Client(1,"jean","rue de la mairie");
			client1.setAdclient(null);
			fail("erreur adclient null");
		} catch(IllegalArgumentException e){}
	}
	
	@Test
	public void testSetAdclientReussi(){
		try{
			Client client1 = new Client(1,"jean","rue de la mairie");
			client1.setAdclient("rue principale");
			
		} catch(IllegalArgumentException e){
			fail("erreur adclient null");}
	}
	
	// Test get et set Idclient
	
	@Test
	public void testGetIdclient(){
		Client client1 = new Client(1,"jean","rue de la mairie");
		assertEquals(1,client1.getIdclient());
	}
	
	@Test
	public void testSetIdclientErreur(){
		try{
			Client client1 = new Client(1,"jean","rue de la mairie");
			client1.setIdclient(-1);
			fail("l'Id est inferieur ou egal a 0");
		} catch(IllegalArgumentException e){}
	}
	
	@Test
	public void testSetIdclientReussi(){
		try{
			Client client1 = new Client(1,"jean","rue de la mairie");
			client1.setIdclient(3);
			
		} catch(IllegalArgumentException e){
			fail("l'Id est inferieur ou egal a 0");}
	}


}
