package testsMetiers;

import static org.junit.Assert.*;
import metiers.Tva;

import org.junit.Test;

public class TestTva {

	
	// Tests Constructeur
	
	@Test
	public void testConstructErreur() {
		
		
		try{
		Tva tva1 = new Tva(1,"normale",-10);
		fail("erreur le taux est inferieur ou egal a 0");
		}
		catch(IllegalArgumentException e){};
	}
	
	
	@Test
	public void testConstructReussi() {
		
		
		try{
		Tva tva1 = new Tva(1,"normale", 20);
		
		}
		catch(IllegalArgumentException e){fail("erreur l'id est inferieur ou egal a 0");};
	}
	
	
	// Test get et set libelle
	
	@Test
	public void testGetlib(){
		Tva tva1 = new Tva(1,"normale",20);
		assertEquals("normale",tva1.getLib());
	}
	
	@Test
	public void testSetLibErreur(){
		try{
			Tva tva1 = new Tva(1,"normale", 20);
			tva1.setLib(null);
			fail("erreur libelle null");
		} catch(IllegalArgumentException e){}
	}
	
	@Test
	public void testSetLibReussi(){
		try{
			Tva tva1 = new Tva(1,"normale", 20);
			tva1.setLib("reduite");
			
		} catch(IllegalArgumentException e){
			fail("erreur libelle null");}
	}
	
	
	// Test get et set Taux
	
	@Test
	public void testGetTaux(){
		Tva tva1 = new Tva(1,"normale",20.0);
		assertEquals(20.0,tva1.getTaux(),0.0d);
	}
	
	@Test
	public void testSetTauxErreur(){
		try{
			Tva tva1 = new Tva(1,"normale",20);
			tva1.setTaux(-20);
			fail("erreur adclient null");
		} catch(IllegalArgumentException e){}
	}
	
	@Test
	public void testSetTauxReussi(){
		try{
			Tva tva1 = new Tva(1,"normale",20);
			tva1.setTaux(35);
			
		} catch(IllegalArgumentException e){
			fail("erreur adclient null");}
	}
	
	
	// Test get et set de l'id
	
	@Test
	public void testGetIdTva(){
		Tva tva1 = new Tva(1,"normale",20.0);
		assertEquals(1,tva1.getIdTva());
	}
	
	@Test
	public void testSetIdTvaErreur(){
		try{
			Tva tva1 = new Tva(1,"normale",20);
			tva1.setIdTva(-20);
			fail("erreur l'id est negatif");
		} catch(IllegalArgumentException e){}
	}
	
	@Test
	public void testSetIdTvaReussi(){
		try{
			Tva tva1 = new Tva(1,"normale",20);
			tva1.setIdTva(14);
			
		} catch(IllegalArgumentException e){
			fail("erreur l'id est negatif");}
	}
	

}