package main;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Logger;

import dao.DAOFactory;
import dao.EPersistance;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.stage.Stage;
import metiers.Produit;
import javafx.scene.control.Button;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;





public class Start extends Application {
	
	@FXML
	TableView<Produit> tabProduit;
	
	@FXML
	TableColumn<Produit,Integer> colRef;
	
	@FXML
	TableColumn<Produit,String> colNom;
	
	@FXML
	TableColumn<Produit,Double> colPrixu;
	
	@FXML
	TableColumn<Produit,Integer> colTP;
	
	@FXML
	Button btnProduitDem;
	
	
	


	
	@FXML
	private void appuiSql(ActionEvent event) {
		
		Parent sql_page_parent;
		try {
			sql_page_parent = FXMLLoader.load(getClass().getResource("../Sql.fxml"));
			Scene sql_page_scene = new Scene(sql_page_parent);
			Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			app_stage.hide();
			app_stage.setScene(sql_page_scene);
			app_stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	
		
	}
	
	@FXML
	private void appuiMemory(ActionEvent event) {
		
		Parent sql_page_parent;
		try {
			sql_page_parent = FXMLLoader.load(getClass().getResource("../Memory.fxml"));
			Scene sql_page_scene = new Scene(sql_page_parent);
			Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			app_stage.hide();
			app_stage.setScene(sql_page_scene);
			app_stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}
	

	@Override
	public void start(Stage primaryStage) {
		
		try {
			  URL fxmlURL=getClass().getResource("acceuil.fxml");
              FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
              Node root = fxmlLoader.load();
              Scene scene = new Scene((AnchorPane) root, 600, 100);
              
              
              primaryStage.setScene(scene);
              primaryStage.setTitle("Interface de gestion de la base de données");
              primaryStage.show();
              } catch (Exception e) {
              e.printStackTrace();
              }

			
		
	}
	
	
	
		
		
	
	public static void main(String[] args) {
		launch(args);
	}
}
