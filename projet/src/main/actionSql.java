package main;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.logging.Logger;

import dao.DAOFactory;
import dao.EPersistance;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.stage.Stage;
import metiers.Client;
import metiers.Facture;
import metiers.Produit;
import metiers.Tva;
import metiers.TypeProduit;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;


public class actionSql {
	
	@FXML
	Button btnPeristance;
	
	@FXML
	private void appuiPersistance(ActionEvent event) {
		
		
		Parent sql_page_parent;
		try {
			sql_page_parent = FXMLLoader.load(getClass().getResource("../Memory.fxml"));
			Scene sql_page_scene = new Scene(sql_page_parent);
			Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			app_stage.hide();
			app_stage.setScene(sql_page_scene);
			app_stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	//Variable produit
	
	@FXML
	Label erreurAddProduit, erreurUpdateProduit, erreurDeleteProduit;
	
	@FXML
	TableView<Produit> tabProduit;
	
	@FXML
	TableColumn<Produit,Integer> colRefProduit;
	
	@FXML
	TableColumn<Produit,String> colNomProduit;
	
	@FXML
	TableColumn<Produit,Double> colPrixuProduit;
	
	@FXML
	TableColumn<Produit,Integer> colTPProduit;
	
	
	@FXML
	Button btnAddProduit,btnUpdateProduit,btnDeleteProduit;
	
	@FXML
	TextField fieldNomAddProduit,fieldPrixuAddProduit,fieldTypeAddProduit;
	
	@FXML
	TextField fieldRefUpdateProduit,fieldNomUpdateProduit,fieldPrixuUpdateProduit,fieldTypeUpdateProduit;
	
	@FXML
	TextField fieldRefDeleteProduit;
	
	//Variable client
	
	@FXML
	Label erreurAddClient, erreurUpdateClient, erreurDeleteClient;
	
	
		@FXML
		TableView<Client> tabClient;
		
		@FXML
		TableColumn<Client,Integer> colIdClient;
		
		@FXML
		TableColumn<Client,String> colNomClient;
		
		@FXML
		TableColumn<Client,String> colAdresseClient;
		
		
//		
//		@FXML
//		Button btnAddClient,btnUpdateClient,btnDeleteClient;
//		
		@FXML
		TextField fieldNomAddClient,fieldAdresseAddClient;
		
		@FXML
		TextField fieldIdUpdateClient,fieldNomUpdateClient,fieldAdresseUpdateClient;
		
		@FXML
		TextField fieldIdDeleteClient;
	
		
		//Variable facture
		
		@FXML
		Label erreurAddFacture, erreurUpdateFacture, erreurDeleteFacture;
		
		
			@FXML
			TableView<Facture> tabFacture;
			
			@FXML
			TableColumn<Facture,Integer> colIdFacture;
			
			@FXML
			TableColumn<Facture,Integer> colIdClientFacture;
			
			@FXML
			TableColumn<Facture,LocalDate> colDateFacture;
			
	
			
			
			
			@FXML
			Button btnAddFacture,btnUpdateFacture,btnDeleteFacture;
			
			@FXML
			TextField fieldIdclAddFacture; 
			
			@FXML
			DatePicker fieldDateAddFacture, fieldDateUpdateFacture, fieldDateDeleteFacture;
			
			@FXML
			TextField fieldIdUpdateFacture,fieldIdClientUpdateFacture;
			
			@FXML
			TextField fieldIdDeleteFacture;
	
			
			
			//Variable Tva
			@FXML
			Label erreurAddTva, erreurUpdateTva, erreurDeleteTva;
			
			
			@FXML
			TableView<Tva> tabTva;
			
			@FXML
			TableColumn<Tva,Integer> colIdTva;
			
			@FXML
			TableColumn<Tva,String> colLibelle;
			
			@FXML
			TableColumn<Tva,Double> colTaux;
			
			
			
			@FXML
			Button btnAddClient,btnUpdateClient,btnDeleteClient;
			
			@FXML
			TextField fieldLibAddTva,fieldTauxAddTva;
			
			@FXML
			TextField fieldIdUpdateTva,fieldLibUpdateTva,fieldTauxUpdateTva;
			
			@FXML
			TextField fieldIdDeleteTva;
			
			
			//Variable TypeProduit
			
			@FXML
			Label erreurAddTP, erreurUpdateTP, erreurDeleteTP;
			
			
			@FXML
			TableView<TypeProduit> tabTypeProduit;
			
			@FXML
			TableColumn<TypeProduit,Integer> colIdTypeProduit;
			
			@FXML
			TableColumn<TypeProduit,String> colType;
			
			
			
			
			
			@FXML
			Button btnAddTypeClient,btnUpdateTypeClient,btnDeleteTypeClient;
			
			@FXML
			TextField fieldTypeAddTypeProduit;
			
			@FXML
			TextField fieldIdUpdateTypeProduit,fieldTypeUpdateTypeProduit;
			
			@FXML
			TextField fieldIdDeleteTypeProduit;
		
	
	@FXML
	protected void initialize(){
		
		//ajout produit
		tabProduit.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getProduitDAO().findAllProduit());
		colRefProduit.setCellValueFactory(new PropertyValueFactory<Produit,Integer>("ref"));
		colNomProduit.setCellValueFactory(new PropertyValueFactory<Produit,String>("nom"));
		colPrixuProduit.setCellValueFactory(new PropertyValueFactory<Produit,Double>("prixu"));
		colTPProduit.setCellValueFactory(new PropertyValueFactory<Produit,Integer>("TypeProduit"));
		
		// ajout client
		tabClient.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getClientDAO().findAllClient());
		colIdClient.setCellValueFactory(new PropertyValueFactory<Client,Integer>("idclient"));
		colNomClient.setCellValueFactory(new PropertyValueFactory<Client,String>("nomclient"));
		colAdresseClient.setCellValueFactory(new PropertyValueFactory<Client,String>("adclient"));
		
		//ajout facture
		tabFacture.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getFactureDAO().findAllFacture());
		colIdFacture.setCellValueFactory(new PropertyValueFactory<Facture,Integer>("idfacture"));
		colIdClientFacture.setCellValueFactory(new PropertyValueFactory<Facture,Integer>("idclient"));
		colDateFacture.setCellValueFactory(new PropertyValueFactory<Facture,LocalDate>("datefacture"));
		
		
		//ajout tva
		tabTva.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getTVADAO().findAllTva());
		colIdTva.setCellValueFactory(new PropertyValueFactory<Tva,Integer>("idTva"));
		colLibelle.setCellValueFactory(new PropertyValueFactory<Tva,String>("lib"));
		colTaux.setCellValueFactory(new PropertyValueFactory<Tva,Double>("taux"));
		
		//ajout type produit
		tabTypeProduit.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getTypeProduitDAO().findAllTypeProduit());
		colIdTypeProduit.setCellValueFactory(new PropertyValueFactory<TypeProduit,Integer>("idtype"));
		colType.setCellValueFactory(new PropertyValueFactory<TypeProduit,String>("type"));				

	}
	
	
	/////////////////////////////////////
	/////							/////
	/////     fonctions produit     /////
	/////							/////
	/////////////////////////////////////
	
	
	DAOFactory daosProduit = DAOFactory.getDAOFactory(EPersistance.MYSQL);
	
	public void appuiAddProduit()
	{
		
		try{
			String nom = fieldNomAddProduit.getText();
			double prixu = Double.valueOf(fieldPrixuAddProduit.getText());
			int type = Integer.valueOf(fieldTypeAddProduit.getText());
			Produit produit = new Produit(nom,prixu,type);
			daosProduit.getProduitDAO().create(produit);
			tabProduit.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getProduitDAO().findAllProduit());
			erreurAddProduit.setText("");

		} catch(IllegalArgumentException e) {erreurAddProduit.setText("Erreur de saisie");}
		
	}
	
	public void appuiDeleteProduit()
	{
		
		try{
	
			int ref = Integer.valueOf(fieldRefDeleteProduit.getText());
			Produit produit = daosProduit.getProduitDAO().getById(ref);
			daosProduit.getProduitDAO().delete(produit);
			tabProduit.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getProduitDAO().findAllProduit());
			erreurDeleteProduit.setText("");
		
		} catch(IllegalArgumentException e ){erreurDeleteProduit.setText("Erreur de saisie");}
	}
	
	public void appuiUpdateProduit()
	{
		
		try{
			
			int ref = Integer.valueOf(fieldRefUpdateProduit.getText());
			String nom = fieldNomUpdateProduit.getText();
			double prixu = Double.valueOf(fieldPrixuUpdateProduit.getText());
			int type = Integer.valueOf(fieldTypeUpdateProduit.getText());
			Produit produit = new Produit(ref,nom,prixu,type);
			daosProduit.getProduitDAO().update(produit);
			tabProduit.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getProduitDAO().findAllProduit());
			erreurUpdateProduit.setText("");

		} catch(IllegalArgumentException e){erreurUpdateProduit.setText("Erreur de saisie");}

	}
	
	
	
	
/////////////////////////////////////
/////							/////
/////     fonctions client      /////
/////							/////
/////////////////////////////////////
	
	
	DAOFactory daosClient = DAOFactory.getDAOFactory(EPersistance.MYSQL);
	
	public void appuiAddClient()
	{
		try{
			
			String nom = fieldNomAddClient.getText();
			String adcl = fieldAdresseAddClient.getText();
			Client client = new Client(nom,adcl);
			daosClient.getClientDAO().create(client);
			tabClient.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getClientDAO().findAllClient());
			erreurAddClient.setText("");
			
		}catch(IllegalArgumentException e){ erreurAddClient.setText("Erreur de saisie"); }
		
	}
	
	
	public void appuiDeleteClient()
	{
		try{
			int Idcl = Integer.valueOf(fieldIdDeleteClient.getText());
			Client client =  daosClient.getClientDAO().getById(Idcl);
			daosClient.getClientDAO().delete(client);
			tabClient.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getClientDAO().findAllClient());
			erreurDeleteClient.setText("");
		
		}catch(IllegalArgumentException e){ erreurDeleteClient.setText("Erreur de saisie");}
	}
	
	
	public void appuiUpdateClient()
	{
		try{
			int idcl = Integer.valueOf(fieldIdUpdateClient.getText());
			String nom = fieldNomUpdateClient.getText();
			String adcl = fieldAdresseUpdateClient.getText();
			Client client = new Client(idcl,nom,adcl);
			daosClient.getClientDAO().update(client);
			tabClient.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getClientDAO().findAllClient());
			erreurUpdateClient.setText("");
			
			
		}catch(IllegalArgumentException e){erreurUpdateClient.setText("Erreur de saisie");			System.out.println("non ok");
}

	} 
	
	
	
/////////////////////////////////////
/////							/////
/////     fonctions Tva         /////
/////							/////
/////////////////////////////////////
	
	DAOFactory daosTva = DAOFactory.getDAOFactory(EPersistance.MYSQL);
	
	public void appuiAddTva()
	{
		try{

			String lib = fieldLibAddTva.getText();
			Double taux = Double.valueOf(fieldTauxAddTva.getText());
			Tva tva = new Tva(lib,taux);
			daosTva.getTVADAO().create(tva);
			tabTva.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getTVADAO().findAllTva());
			erreurAddTva.setText("");
			
		}catch(IllegalArgumentException e){erreurAddTva.setText("Erreur de saisie");}
	}
	
	public void appuiDeleteTva()
	{
		try{
			
			int Idtva = Integer.valueOf(fieldIdDeleteTva.getText());
			Tva tva = daosTva.getTVADAO().getById(Idtva);
			daosTva.getTVADAO().delete(tva);
			tabTva.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getTVADAO().findAllTva());
			erreurDeleteTva.setText("");
			
		}catch(IllegalArgumentException e){erreurDeleteTva.setText("Erreur de saisie");}
	}
	
	public void appuiUpdateTva()
	{
		try{
			
			int idtva = Integer.valueOf(fieldIdUpdateTva.getText());
			String lib = fieldLibUpdateTva.getText();
			Double taux = Double.valueOf(fieldTauxUpdateTva.getText());
			Tva tva = new Tva(idtva,lib,taux);
			daosTva.getTVADAO().update(tva);
			tabTva.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getTVADAO().findAllTva());
			erreurUpdateTva.setText("");
			
		}catch(IllegalArgumentException e){	erreurUpdateTva.setText("Erreur de saisie");}


	} 

	
/////////////////////////////////////
/////							/////
/////     fonctions Facture     /////
/////							/////
/////////////////////////////////////
	DAOFactory daosFacture = DAOFactory.getDAOFactory(EPersistance.MYSQL);


	public void appuiAddFacture()
	{
		try{
			Integer idcl = Integer.valueOf(fieldIdclAddFacture.getText());
			LocalDate date = fieldDateAddFacture.getValue();
			Facture facture = new Facture(idcl,date);
			daosFacture.getFactureDAO().create(facture);
			tabFacture.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getFactureDAO().findAllFacture());
			erreurAddFacture.setText("");

		}catch(IllegalArgumentException e){erreurAddFacture.setText("Erreur de saisie");}

	}

	public void appuiDeleteFacture()
	{
		try{
			int Id = Integer.valueOf(fieldIdDeleteFacture.getText());
			Facture facture = daosFacture.getFactureDAO().getById(Id);;
			daosFacture.getFactureDAO().delete(facture);
			tabFacture.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getFactureDAO().findAllFacture());
			erreurDeleteFacture.setText("");
			
		}catch(IllegalArgumentException e){erreurDeleteFacture.setText("Erreur de saisie");}
	}

	public void appuiUpdateFacture()
	{
		try{
			int idfac = Integer.valueOf(fieldIdUpdateFacture.getText());
			int idcl = Integer.valueOf(fieldIdClientUpdateFacture.getText());
			LocalDate date = fieldDateUpdateFacture.getValue();
			Facture facture = new Facture(idfac,idcl,date);
			daosFacture.getFactureDAO().update(facture);
			tabFacture.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getFactureDAO().findAllFacture());
			erreurUpdateFacture.setText("");
			
		}catch(IllegalArgumentException e){erreurUpdateFacture.setText("Erreur de saisie");}

	} 

/////////////////////////////////////
/////							/////
/////  fonctions TypeProduit    /////
/////							/////
/////////////////////////////////////

	DAOFactory daosTypeProduit = DAOFactory.getDAOFactory(EPersistance.MYSQL);

	public void appuiAddTypeProduit()
	{
		try{
			
			String type = fieldTypeAddTypeProduit.getText();
			TypeProduit tp = new TypeProduit(type);
			daosTypeProduit.getTypeProduitDAO().create(tp);
			tabTypeProduit.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getTypeProduitDAO().findAllTypeProduit());
			erreurAddTP.setText("");
		
		}catch(IllegalArgumentException e){erreurAddTP.setText("Erreur de saisie");}
	}

	public void appuiDeleteTypeProduit()
	{
		try{
			int Id = Integer.valueOf(fieldIdDeleteTypeProduit.getText());
			TypeProduit tp = daosTypeProduit.getTypeProduitDAO().getById(Id);
			daosTypeProduit.getTypeProduitDAO().delete(tp);
			tabTypeProduit.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getTypeProduitDAO().findAllTypeProduit());
			erreurDeleteTP.setText("");
			
		}catch(IllegalArgumentException e){erreurDeleteTP.setText("Erreur de saisie");}
	}

	public void appuiUpdateTypeProduit()
	{
		try{
			int id = Integer.valueOf(fieldIdUpdateTypeProduit.getText());
			String type = fieldTypeUpdateTypeProduit.getText();
			TypeProduit tp = new TypeProduit(id,type);
			daosTypeProduit.getTypeProduitDAO().update(tp);
			tabTypeProduit.getItems().setAll(DAOFactory.getDAOFactory(EPersistance.MYSQL).getTypeProduitDAO().findAllTypeProduit());
			erreurUpdateTP.setText("");
			
		}catch(IllegalArgumentException e){erreurUpdateTP.setText("Erreur de saisie");}

} 

}
