package testMyMemory;

import static org.junit.Assert.*;

import org.junit.Test;

import metiers.Tva;
import metiers.TypeProduit;

import dao.DAOFactory;
import dao.EPersistance;

public class TestMyMemoryTypeProduit {
	
	DAOFactory daosTP = DAOFactory.getDAOFactory(dao.EPersistance.MEMORY);


	@Test
	public void testAddTypeProduit(){
		
		int size  = daosTP.getTypeProduitDAO().findAllTypeProduit().size();
		TypeProduit tp = new TypeProduit("vehicule");
		daosTP.getTypeProduitDAO().create(tp);
		assertEquals(size + 1 , daosTP.getTypeProduitDAO().findAllTypeProduit().size());
		
	}
	
	@Test
	public void testDeleteTypeProduit()
	{
		TypeProduit TypeProduit = new TypeProduit("Patisserie");
		int id = daosTP.getTypeProduitDAO().create(TypeProduit);
		TypeProduit.setIdtype(id);
		int size  = daosTP.getTypeProduitDAO().findAllTypeProduit().size();
		daosTP.getTypeProduitDAO().delete(TypeProduit);
		assertEquals(size - 1 , daosTP.getTypeProduitDAO().findAllTypeProduit().size());

	}
	
	@Test
	public void testUpdateTypeProduit()
	{
		DAOFactory daosTypeProduit = DAOFactory.getDAOFactory(EPersistance.MEMORY);
		TypeProduit TypeProduit1 = daosTypeProduit.getTypeProduitDAO().getById(1);
		TypeProduit TPSauvegarde = new TypeProduit(TypeProduit1.getIdtype(),TypeProduit1.getType());
		TypeProduit TypeProduit2 = new TypeProduit(1,"Vehicule");
		daosTypeProduit.getTypeProduitDAO().update(TypeProduit2);
		assertNotEquals(TPSauvegarde,daosTypeProduit.getTypeProduitDAO().getById(1));
		daosTypeProduit.getTypeProduitDAO().update(TypeProduit1);
	}
}