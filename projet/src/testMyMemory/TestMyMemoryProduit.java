package testMyMemory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import dao.DAOFactory;
import dao.EPersistance;
import dao.InterfaceGenericProduitDao;
import metiers.Client;
import metiers.Produit;


public class TestMyMemoryProduit {
	

		
		DAOFactory daosProduit = DAOFactory.getDAOFactory(EPersistance.MEMORY);
	

	@Test
	public void testAddProduit(){
		
		int size  = daosProduit.getProduitDAO().findAllProduit().size();
		Produit produit = new Produit(9,"zingraff",12.0,10);
		daosProduit.getProduitDAO().create(produit);
		assertEquals(size + 1 , daosProduit.getProduitDAO().findAllProduit().size());
}

	@Test
	public void testDeleteProduit()
	{
		Produit produit = new Produit("Casquette",30.2,4);
		int id = daosProduit.getProduitDAO().create(produit);
		produit.setRef(id);
		int size  = daosProduit.getProduitDAO().findAllProduit().size();
		daosProduit.getProduitDAO().delete(produit);
		assertEquals(size - 1 , daosProduit.getProduitDAO().findAllProduit().size());

	}
	
	
	
	@Test
	public void testUpdateProduit()
	{
		DAOFactory daosProduit = DAOFactory.getDAOFactory(EPersistance.MEMORY);
		Produit produit1 = daosProduit.getProduitDAO().getById(1);
		Produit produitSauvegarde = new Produit(produit1.getRef(),produit1.getNom(),produit1.getPrixu(),produit1.getTypeProduit());
		Produit produit2 = new Produit(1,"Voiture",30.2,4);
		daosProduit.getProduitDAO().update(produit2);
		assertNotEquals(produitSauvegarde,daosProduit.getProduitDAO().getById(1));
		daosProduit.getProduitDAO().update(produit1);
	}
		
	}
		

