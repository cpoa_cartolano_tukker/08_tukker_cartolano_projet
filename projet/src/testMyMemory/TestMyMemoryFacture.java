package testMyMemory;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import org.junit.Test;
import metiers.Facture;
import metiers.Produit;
import dao.DAOFactory;
import dao.EPersistance;

public class TestMyMemoryFacture {
	
	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(
	        FormatStyle.MEDIUM).withLocale(Locale.GERMAN);

	
	DAOFactory daosFacture = DAOFactory.getDAOFactory(dao.EPersistance.MEMORY);


	@Test
	public void testAddFacture(){
		
		int size  = daosFacture.getFactureDAO().findAllFacture().size();
		Facture facture = new Facture(7,LocalDate.parse("24.12.2014", germanFormatter));
		daosFacture.getFactureDAO().create(facture);
		assertEquals(size + 1 , daosFacture.getFactureDAO().findAllFacture().size());
	
		}
	
	@Test
	public void testDeleteFacture()
	{
		Facture Facture = new Facture(6,LocalDate.parse("24.12.2014", germanFormatter));
		int id = daosFacture.getFactureDAO().create(Facture);
		Facture.setIdfacture(id);
		int size  = daosFacture.getFactureDAO().findAllFacture().size();
		daosFacture.getFactureDAO().delete(Facture);
		assertEquals(size - 1 , daosFacture.getFactureDAO().findAllFacture().size());

	}
	
	@Test
	public void testUpdateFacture()
	{
		DAOFactory daosFacture = DAOFactory.getDAOFactory(EPersistance.MEMORY);
		Facture Facture1 = daosFacture.getFactureDAO().getById(1);
		Facture FactureSauvegarde = new Facture(Facture1.getIdfacture(),Facture1.getIdclient(),Facture1.getDatefacture());
		Facture Facture2 = new Facture(1,2,LocalDate.parse("24.12.2014", germanFormatter));
		daosFacture.getFactureDAO().update(Facture2);
		assertNotEquals(FactureSauvegarde,daosFacture.getFactureDAO().getById(1));
		daosFacture.getFactureDAO().update(Facture1);
	}
		
}