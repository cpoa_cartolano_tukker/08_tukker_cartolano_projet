package testMyMemory;

import static org.junit.Assert.*;

import org.junit.Test;

import metiers.Produit;
import metiers.Tva;

import dao.DAOFactory;
import dao.EPersistance;

public class TestMyMemoryTva {
	
	DAOFactory daosTva = DAOFactory.getDAOFactory(dao.EPersistance.MEMORY);


	@Test
	public void testAddTva(){
		
		int size  = daosTva.getTVADAO().findAllTva().size();
		Tva tva = new Tva("reduite",10.2);
		daosTva.getTVADAO().create(tva);
		assertEquals(size + 1 , daosTva.getTVADAO().findAllTva().size());
		
	}
	
	@Test
	public void testDeleteTva()
	{
		Tva Tva = new Tva("normale",20.2);
		int id = daosTva.getTVADAO().create(Tva);
		Tva.setIdTva(id);
		int size  = daosTva.getTVADAO().findAllTva().size();
		daosTva.getTVADAO().delete(Tva);
		assertEquals(size - 1 , daosTva.getTVADAO().findAllTva().size());

	}
	
	@Test
	public void testUpdateTva()
	{
		DAOFactory daosTva = DAOFactory.getDAOFactory(EPersistance.MEMORY);
		Tva Tva1 = daosTva.getTVADAO().getById(1);
		Tva TvaSauvegarde = new Tva(Tva1.getIdTva(),Tva1.getLib(),Tva1.getTaux());
		Tva Tva2 = new Tva(1,"reduite",10.2);
		daosTva.getTVADAO().update(Tva2);
		assertNotEquals(TvaSauvegarde,daosTva.getTVADAO().getById(1));
		daosTva.getTVADAO().update(Tva1);
	}
		
}