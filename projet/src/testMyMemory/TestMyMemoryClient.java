package testMyMemory;

import static org.junit.Assert.*;

import org.junit.Test;
import metiers.Client;
import metiers.Produit;
import dao.DAOFactory;
import dao.EPersistance;

public class TestMyMemoryClient {
	
	DAOFactory daosClient = DAOFactory.getDAOFactory(dao.EPersistance.MEMORY);


	@Test
	public void testAddClient(){
		
		
		int size  = daosClient.getClientDAO().findAllClient().size();
		Client client = new Client("Zingraff","rue de la mairie");
		int id = daosClient.getClientDAO().create(client);
		assertEquals(size + 1 , daosClient.getClientDAO().findAllClient().size());
		assertEquals(client,daosClient.getClientDAO().getById(id));

		

		
	}

	@Test
	public void testDeleteClient()
	{
		
		Client Client = new Client("Alexis","Rue de Ladoucette");
		int id = daosClient.getClientDAO().create(Client);
		Client.setIdclient(id);
		int size  = daosClient.getClientDAO().findAllClient().size();
		daosClient.getClientDAO().delete(Client);
		assertEquals(size - 1 , daosClient.getClientDAO().findAllClient().size());

		

	}
	
	@Test
	public void testUpdateClient()
	{
		Client Client1 = daosClient.getClientDAO().getById(1);
		Client ClientSauvegarde = new Client(Client1.getIdclient(),Client1.getNomclient(),Client1.getAdclient());
		Client Client2 = new Client(1,"Anthony","Rue de l'eglise");
		daosClient.getClientDAO().update(Client2);
		assertNotEquals(ClientSauvegarde,daosClient.getClientDAO().getById(1));
		daosClient.getClientDAO().update(Client1);
	}
		
}
