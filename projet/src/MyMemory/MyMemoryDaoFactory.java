package MyMemory;

import dao.DAOFactory;
import dao.InterfaceGenericClientDao;
import dao.InterfaceGenericFactureDao;
import dao.InterfaceGenericProduitDao;
import dao.InterfaceGenericTvaDao;
import dao.InterfaceGenericTypeProduitDao;

public class MyMemoryDaoFactory extends DAOFactory {

	@Override
	public InterfaceGenericTvaDao getTVADAO() {		
		return (InterfaceGenericTvaDao) ListeMemoireTvaDAO.getInstance();
	}
	
	public InterfaceGenericTypeProduitDao getTypeProduitDAO() {		
		return (InterfaceGenericTypeProduitDao) ListeMemoireTypeProduitDAO.getInstance();
	}
	
	public InterfaceGenericProduitDao getProduitDAO() {		
		return (InterfaceGenericProduitDao) ListeMemoireProduitDAO.getInstance();
	}
	
	public InterfaceGenericFactureDao getFactureDAO() {		
		return (InterfaceGenericFactureDao) ListeMemoireFactureDAO.getInstance();
	}
	
	public InterfaceGenericClientDao getClientDAO() {		
		return (InterfaceGenericClientDao) ListeMemoireClientDAO.getInstance();
	}
}
