package MyMemory;

import java.util.ArrayList;
import java.util.List;



//import dao.DAOFactory;
//import dao.EPersistance;
import dao.InterfaceGenericTvaDao;
import metiers.Client;
import metiers.Produit;
import metiers.Tva;

public class ListeMemoireTvaDAO implements InterfaceGenericTvaDao {

	private static ListeMemoireTvaDAO instance;

	private List<Tva> donnees;
	
	//private InterfaceGenericTvaDao dao;
	
	

	public static ListeMemoireTvaDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoireTvaDAO();
		}

		return instance;
	}

	private ListeMemoireTvaDAO() {

		this.donnees = new ArrayList<Tva>();

		this.donnees.add(new Tva(1,"Normale", 20.00));
		this.donnees.add(new Tva(2,"Réduite", 10.00));
		this.donnees.add(new Tva(3,"Normale", 20.00));
		this.donnees.add(new Tva(4,"Réduite", 10.00));
		this.donnees.add(new Tva(5,"Normale", 20.00));
		this.donnees.add(new Tva(6,"Réduite", 10.00));
		this.donnees.add(new Tva(7,"Normale", 20.00));
		this.donnees.add(new Tva(8,"Réduite", 10.00));
		
	}

	public int create(Tva objet) {

		// TODO voir le cours 0 pour que contains donne le r�sultat escompt� !
				if (this.donnees.contains(objet)) {
					throw new IllegalArgumentException("Tentative d'insertion d'un doublon");
				} else {
					this.donnees.add(objet);
					objet.setIdTva(donnees.size());

				}
				return donnees.indexOf(objet)+1;
	}

	public void update(Tva objet) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		Tva tva = this.donnees.get(objet.getIdTva()-1);
		if (tva == null) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			tva.setLib(objet.getLib());
			tva.setTaux(objet.getTaux());
		}
	}

	public void delete(Tva objet) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}

	public Tva getById(int id) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		Tva tva = null;
		try{
			tva = donnees.get(id-1);
		}catch(IndexOutOfBoundsException e){
			throw new IllegalArgumentException("Aucun objet ne possède cet identifiant");

		};
		return tva;
	}

	public List<Tva> findAllTva() {

		return this.donnees;
	}
	
	
	

}