package MyMemory;

import java.util.ArrayList;
import java.util.List;

import dao.InterfaceGenericProduitDao;
import metiers.Produit;


public class ListeMemoireProduitDAO implements InterfaceGenericProduitDao {

	private static ListeMemoireProduitDAO instance;

	private List<Produit> donnees;

	public static ListeMemoireProduitDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoireProduitDAO();
		}

		return instance;
	}

	private ListeMemoireProduitDAO() {

		this.donnees = new ArrayList<Produit>();

		this.donnees.add(new Produit(1,"Chaise", 19.90,1));
		this.donnees.add(new Produit(2,"Table",99.90,1));
		this.donnees.add(new Produit(3,"Meuble Tv", 45.90,1));
		this.donnees.add(new Produit(4,"Canape",699.90,1));
		this.donnees.add(new Produit(5,"Casquette", 20,2));
		this.donnees.add(new Produit(6,"Pentalon",70,2));
		this.donnees.add(new Produit(7,"Chemise", 50,2));
		this.donnees.add(new Produit(8,"Ceinture",10,2));
	}

	public int create(Produit objet) {

		// TODO voir le cours 0 pour que contains donne le r�sultat escompt� !
		if (this.donnees.contains(objet)) {
			throw new IllegalArgumentException("Tentative d'insertion d'un doublon");
		} else {
			this.donnees.add(objet);
			objet.setRef(donnees.size());
		}
		return donnees.indexOf(objet);
	}

	public void update(Produit objet) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		Produit produit = this.donnees.get(objet.getRef()-1);
		if (produit == null) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			produit.setNom(objet.getNom());
			produit.setPrixu(objet.getPrixu());
			produit.setTypeProduit(objet.getTypeProduit());

		}
	}

	public void delete(Produit objet) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}

	public Produit getById(int id) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		Produit produit = null;
		try{
			produit = donnees.get(id-1);
		}catch(IndexOutOfBoundsException e){
			throw new IllegalArgumentException("Aucun objet ne possède cet identifiant");

		};
		return produit;
	}

	public List<Produit> findAllProduit() {

		return this.donnees;
	}

	
}

	
