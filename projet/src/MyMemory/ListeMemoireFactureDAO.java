package MyMemory;




import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import dao.InterfaceGenericFactureDao;
import metiers.Client;
import metiers.Facture;


public class ListeMemoireFactureDAO implements InterfaceGenericFactureDao {

	private static ListeMemoireFactureDAO instance;

	private List<Facture> donnees;

	public static ListeMemoireFactureDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoireFactureDAO();
		}

		return instance;
	}

	private ListeMemoireFactureDAO() {
		
		this.donnees = new ArrayList<Facture>();

		DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(
		        FormatStyle.MEDIUM).withLocale(Locale.GERMAN);


		this.donnees.add(new Facture(1,12,LocalDate.parse("24.12.2014", germanFormatter)));
		this.donnees.add(new Facture(2,31,LocalDate.parse("21.10.2016", germanFormatter)));
		this.donnees.add(new Facture(3,4,LocalDate.parse("12.11.2014", germanFormatter)));
		this.donnees.add(new Facture(4,6,LocalDate.parse("19.12.2015", germanFormatter)));
		this.donnees.add(new Facture(5,91,LocalDate.parse("10.09.2010", germanFormatter)));
		this.donnees.add(new Facture(6,11,LocalDate.parse("04.07.2009", germanFormatter)));
		this.donnees.add(new Facture(7,34,LocalDate.parse("11.09.2000", germanFormatter)));
		this.donnees.add(new Facture(8,65,LocalDate.parse("02.12.2000", germanFormatter)));
	} 

	public int create(Facture objet) {

		// TODO voir le cours 0 pour que contains donne le résultat escompté !
		if (this.donnees.contains(objet)) {
			throw new IllegalArgumentException("Tentative d'insertion d'un doublon");
		} else {
			this.donnees.add(objet);
			objet.setIdfacture(donnees.size());

		}
		return donnees.indexOf(objet)+1;
	}

	public void update(Facture objet) {

		// TODO voir le cours 0 pour que indexOf donne le résultat escompté !
		Facture facture = this.donnees.get(objet.getIdfacture()-1);
		if (facture == null) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			facture.setIdclient(objet.getIdclient());
			facture.setDatefacture(objet.getDatefacture());		}
	
			
		}
	

	public void delete(Facture objet) {

		// TODO voir le cours 0 pour que indexOf donne le résultat escompté !
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}

	public Facture getById(int id) {

		Facture facture = null;
		try{
			facture = donnees.get(id-1);
		}catch(IndexOutOfBoundsException e){
			throw new IllegalArgumentException("Aucun objet ne possède cet identifiant");

		};
		return facture;
	}

	

	@Override
	public List<Facture> findAllFacture() {
		return this.donnees;
	}

	

	

}