package MyMemory;

import java.util.ArrayList;
import java.util.List;

import dao.InterfaceGenericClientDao;
import metiers.Client;

public class ListeMemoireClientDAO implements InterfaceGenericClientDao {

	private static ListeMemoireClientDAO instance;

	private List<Client> donnees;

	public static ListeMemoireClientDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoireClientDAO();
		}

		return instance;
	}

	private ListeMemoireClientDAO() {

		this.donnees = new ArrayList<Client>();

		this.donnees.add(new Client(1,"Theo","12 rue de la mairie"));
		this.donnees.add(new Client(2,"Jules","9 rue principale"));
		this.donnees.add(new Client(3,"Alexis","54 avenue des champs"));
		this.donnees.add(new Client(4,"Fabio","2 rue Mozart"));
		this.donnees.add(new Client(5,"Anthony","23 rue du parc"));
		this.donnees.add(new Client(6,"Lucas","10 rue de l'eglise"));
		this.donnees.add(new Client(7,"Thibaut","41 rue Serpenoise"));
		this.donnees.add(new Client(8,"Pauline","6 rue de Ladoucette"));
	}

	public int create(Client objet) {

		// TODO voir le cours 0 pour que contains donne le r�sultat escompt� !
		if (this.donnees.contains(objet)) {
			throw new IllegalArgumentException(donnees.toString());
		} else {
			this.donnees.add(objet);
			objet.setIdclient(donnees.size());
		}
		return donnees.indexOf(objet)+1;
	}

	public void update(Client objet) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		Client client = this.donnees.get(objet.getIdclient()-1);
		if (client == null) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			client.setNomclient(objet.getNomclient());
			client.setAdclient(objet.getAdclient());

		}
	}

	public void delete(Client objet) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}

	public Client getById(int id) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		Client client = null;
		try{
			client = donnees.get(id-1);
		}catch(IndexOutOfBoundsException e){
			throw new IllegalArgumentException("Aucun objet ne possède cet identifiant");

		};
		return client;
		
		
	}

	

	@Override
	public List<Client> findAllClient() {
		return this.donnees;
	}

	

}