package MyMemory;

import java.util.ArrayList;
import java.util.List;

import dao.InterfaceGenericTypeProduitDao;
import metiers.Client;
import metiers.Produit;
import metiers.TypeProduit;

public class ListeMemoireTypeProduitDAO implements InterfaceGenericTypeProduitDao {

	private static ListeMemoireTypeProduitDAO instance;

	private List<TypeProduit> donnees;

	public static ListeMemoireTypeProduitDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoireTypeProduitDAO();
		}

		return instance;
	}

	private ListeMemoireTypeProduitDAO() {

		this.donnees = new ArrayList<TypeProduit>();

		this.donnees.add(new TypeProduit(1,"Meuble"));
		this.donnees.add(new TypeProduit(2,"Vetements"));
		this.donnees.add(new TypeProduit(3,"Vehicule"));
		this.donnees.add(new TypeProduit(4,"Alimentaire"));
		this.donnees.add(new TypeProduit(5,"Meuble"));
		this.donnees.add(new TypeProduit(6,"Vetements"));
		this.donnees.add(new TypeProduit(7,"Vehicule"));
		this.donnees.add(new TypeProduit(8,"Alimentaire"));
	}

	public int create(TypeProduit objet) {

		// TODO voir le cours 0 pour que contains donne le r�sultat escompt� !
		if (this.donnees.contains(objet)) {
			throw new IllegalArgumentException("Tentative d'insertion d'un doublon");
		} else {
			this.donnees.add(objet);
			objet.setIdtype(donnees.size());

		}
		
		return donnees.indexOf(objet)+1;
	}

	public void update(TypeProduit objet) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		TypeProduit TypeProduit = this.donnees.get(objet.getIdtype()-1);
		if (TypeProduit == null) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			TypeProduit.setType(objet.getType());
		}
	}

	public void delete(TypeProduit objet) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}

	public TypeProduit getById(int id) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		TypeProduit TypeProduit = null;
		try{
			TypeProduit = donnees.get(id-1);
		}catch(IndexOutOfBoundsException e){
			throw new IllegalArgumentException("Aucun objet ne possède cet identifiant");

		};
		return TypeProduit;
	}

	

	@Override
	public List<TypeProduit> findAllTypeProduit() {
		return this.donnees;
	}

	

}