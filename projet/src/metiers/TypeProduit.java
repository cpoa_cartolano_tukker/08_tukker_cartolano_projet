package metiers;

public class TypeProduit {
	
	private int idtype;
	private String type;
	//tva
	
	public TypeProduit(int idtype, String type) {
		super();
		setIdtype( idtype);
		setType(type);
		
	}
	
	public TypeProduit(int idtype) {
		setIdtype( idtype);
		
	}
	
	public TypeProduit(String type) {
		super();
		
		setType(type);
		
	}

	public int getIdtype() {
		return idtype;
	}

	public void setIdtype(int idtype) {
		this.idtype = idtype;
		
		if (idtype<=0)
		{
			throw new IllegalArgumentException("erreur l'id type inférieur à 0");
		}
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		if (type==null || type.isEmpty())
		{
			throw new IllegalArgumentException("erreur le type est vide");
		}
		this.type = type;
	}

	@Override
	public String toString() {
		return "Type_produit [id_type=" + idtype + ", type=" + type + "]";
	}
	
	

}
