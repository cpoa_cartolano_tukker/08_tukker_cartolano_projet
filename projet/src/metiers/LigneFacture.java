package metiers;

public class LigneFacture {
	
	private int idLf;
	private int nbprod;
	private double prixT;
	private int qte;
	
	
	@Override
	public String toString() {
		return "LigneFacture [idLf=" + idLf + ", nbprod=" + nbprod + ", prixT=" + prixT + ", qte=" + qte + "]";
	}
	
	
	public LigneFacture(int idLf, int nbprod, double prixT, int qte) {
		super();
		this.idLf = idLf;
		this.nbprod = nbprod;
		this.prixT = prixT;
		this.qte = qte;
	}
	public int getIdLf() {
		return idLf;
	}
	public void setIdLf(int idLf) {
		this.idLf = idLf;
	}
	public int getNbprod() {
		return nbprod;
	}
	public void setNbprod(int nbprod) {
		this.nbprod = nbprod;
	}
	public double getPrixT() {
		return prixT;
	}
	public void setPrixT(double prixT) {
		this.prixT = prixT;
	}
	public int getQte() {
		return qte;
	}
	public void setQte(int qte) {
		this.qte = qte;
	}
	

}
