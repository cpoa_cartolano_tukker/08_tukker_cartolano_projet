package metiers;

public class Client {
	
	private int idclient;
	private String nomclient;
	private String adclient;
	
	public Client(int idclient, String nomclient, String adclient) {
		super();
		setIdclient(idclient);
		setNomclient( nomclient);
		setAdclient( adclient);
		
	}
	
	public Client(int idclient) {
		
		setIdclient(idclient);
		
	}
	
	public Client(String nomclient, String adclient) {
		super();
		
		setNomclient( nomclient);
		setAdclient( adclient);
		
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adclient == null) ? 0 : adclient.hashCode());
		result = prime * result + idclient;
		result = prime * result + ((nomclient == null) ? 0 : nomclient.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (adclient == null) {
			if (other.adclient != null)
				return false;
		} else if (!adclient.equals(other.adclient))
			return false;
		if (idclient != other.idclient)
			return false;
		if (nomclient == null) {
			if (other.nomclient != null)
				return false;
		} else if (!nomclient.equals(other.nomclient))
			return false;
		return true;
	}

	public int getIdclient() {
		return idclient;
	}

	public void setIdclient(int idclient) {
		
		
		if (idclient<=0)
		{
			throw new IllegalArgumentException("erreur idclient inférieur à 0");
		}
		this.idclient = idclient;
	}

	public String getNomclient() {
		
		return nomclient;
	}

	public void setNomclient(String nomclient) {
		if (nomclient==null || nomclient.isEmpty())
		{
			throw new IllegalArgumentException("erreur idclient inférieur à 0");
		}
		this.nomclient = nomclient;
	}

	public String getAdclient() {
		return adclient;
	}

	public void setAdclient(String adclient) {
		if (adclient==null || nomclient.isEmpty())
		{
			throw new IllegalArgumentException("erreur idclient inférieur à 0");
		}
		this.adclient = adclient;
	}

	@Override
	public String toString() {
		return "Client [idclient=" + idclient + ", nomclient=" + nomclient + ", adclient=" + adclient + "]";
	}
	
	

}
