package metiers;

import java.time.LocalDate;
import java.util.HashMap;

public class Facture {
	
	private int idclient;
	private int idfacture;
	private LocalDate datefacture;
	//private HashMap<Produit,Integer> ligneProduit;
	
	
	public Facture(int idfacture, int idclient, LocalDate datefacture ) {
		super();
		setIdfacture(idfacture);
		setDatefacture(datefacture);
		setIdclient(idclient);
		
	}
	
	

	public Facture(int idfacture) {
		
		setIdfacture(idfacture);
		
		
	
		
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((datefacture == null) ? 0 : datefacture.hashCode());
		result = prime * result + idclient;
		result = prime * result + idfacture;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Facture other = (Facture) obj;
		if (datefacture == null) {
			if (other.datefacture != null)
				return false;
		} else if (!datefacture.equals(other.datefacture))
			return false;
		if (idclient != other.idclient)
			return false;
		if (idfacture != other.idfacture)
			return false;
		return true;
	}
	public Facture(int idclient, LocalDate datefacture ) {
		super();
		
		setDatefacture(datefacture);
		setIdclient(idclient);
		
	
		
	}

	public int getIdfacture() {
		return idfacture;
	}

	public void setIdfacture(int idfacture) {
		this.idfacture = idfacture;
		if (idfacture<=0)
		{
			throw new IllegalArgumentException("erreur la ref est inférieur à 0");
		}
		
	}

	public LocalDate getDatefacture() {
		return datefacture;
	}

	public void setDatefacture(LocalDate datefacture) {
		this.datefacture = datefacture;
		if (datefacture==null)
		{
			throw new IllegalArgumentException("erreur le nom est vide");
		}
	}

	
	
	public int getIdclient() {
		return idclient;
	}

	public void setIdclient(int idclient) {
		this.idclient = idclient;
		if (idclient<=0)
		{
			throw new IllegalArgumentException("erreur la ref est inférieur à 0");
		}
	}

	@Override
	public String toString() {
		return "Facture [idclient=" + idclient + ", idfacture=" + idfacture + ", datefacture=" + datefacture + "]";
	}

	
	

}
