package metiers;

public class Tva {
	private int IdTva;
	private String lib;
	private double taux;
	
	public Tva(int IdTva, String lib, double taux) {
		super();
		setLib(lib);
		setTaux(taux);
		setIdTva( IdTva);
		
		
	}
	
	public Tva(int IdTva) {
		
		setIdTva( IdTva);
		
		
	}
	
	public Tva(String lib, double taux) {
		super();
		setLib(lib);
		setTaux(taux);
		
		
	}

	public int getIdTva() {
		return IdTva;
	}

	public void setIdTva(int idTva) {
		
		if (idTva<=0)
		{
			throw new IllegalArgumentException("erreur le taux est inférieur à 0");
		}
		IdTva = idTva;
	}

	public String getLib() {
		return lib;
	}

	public void setLib(String lib) {
		
		if (lib==null || lib.isEmpty())
		{
			throw new IllegalArgumentException("erreur le libelle est vide");
		}
		
		this.lib = lib;
	}

	public double getTaux() {
		return taux;
	}

	public void setTaux(double taux) {
		
		if (taux<=0)
		{
			throw new IllegalArgumentException("erreur le taux est inférieur à 0");
		}
		
		this.taux = taux;
	}

	@Override
	public String toString() {
		return "Tva [lib=" + lib + ", taux=" + taux + "]";
	}
	
	

}
