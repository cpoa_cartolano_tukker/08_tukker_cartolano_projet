package metiers;

public class Produit {
	
	private int ref;
	private String nom;
	private double prixu;
	private int TypeProduit;
	
	public Produit(int ref, String nom, double prixu, int TypeProduit) {
		
		setRef( ref);
		setNom(nom);
		setPrixu  (prixu);
		setTypeProduit( TypeProduit);
		
		
	}
	
public Produit(int ref) {
		
		setRef( ref);
		
		
		
	}
public Produit(String nom, double prixu, int TypeProduit) {
		
		
		setNom(nom);
		setPrixu  (prixu);
		setTypeProduit( TypeProduit);
		
		
	}

	@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + TypeProduit;
	result = prime * result + ((nom == null) ? 0 : nom.hashCode());
	long temp;
	temp = Double.doubleToLongBits(prixu);
	result = prime * result + (int) (temp ^ (temp >>> 32));
	result = prime * result + ref;
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Produit other = (Produit) obj;
	if (TypeProduit != other.TypeProduit)
		return false;
	if (nom == null) {
		if (other.nom != null)
			return false;
	} else if (!nom.equals(other.nom))
		return false;
	if (Double.doubleToLongBits(prixu) != Double.doubleToLongBits(other.prixu))
		return false;
	if (ref != other.ref)
		return false;
	return true;
}
	public int getRef() {
		return ref;
	}

	public void setRef(int ref) {
		if (ref<=0)
		{
			throw new IllegalArgumentException("erreur la ref est inférieur à 0");
		}
		
		this.ref = ref;
	}

	public String getNom() {
		
		return nom;
	}

	public void setNom(String nom) {
		if (nom==null || nom.isEmpty())
		{
			throw new IllegalArgumentException("erreur le nom est vide");
		}
		this.nom = nom;
	}

	public double getPrixu() {
		return prixu;
	}

	public void setPrixu(double prixu) {
		if (prixu<=0)
		{
			throw new IllegalArgumentException("erreur le prix unitaire est inférieur à 0");
		}
		this.prixu = prixu;
	}

	public int getTypeProduit() {
		return TypeProduit;
	}

	public void setTypeProduit(int TypeProduit) {
		if (TypeProduit<= 0) 
		{
			throw new IllegalArgumentException("erreur le type de produit est vide");
		}
		
		this.TypeProduit = TypeProduit;
	}

	@Override
	public String toString() {
		return "ref=" + ref + " / nom = " + nom + " / prixu=" + prixu + " / TypeProduit=" + TypeProduit ;
	}

	public String AfficherRef(){
		return " ref = " + ref;
	}
	
	
	
	
}
