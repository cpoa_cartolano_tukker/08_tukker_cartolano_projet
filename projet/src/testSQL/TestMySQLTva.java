package testSQL;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Scanner;

import org.junit.Test;

import dao.DAOFactory;
import dao.EPersistance;
import metiers.Produit;
import metiers.Tva;
import metiers.TypeProduit;

public class TestMySQLTva {
	
	@Test
	public void testAddTva(){
		
		DAOFactory daosTva = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		int size  = daosTva.getTVADAO().findAllTva().size();
		Tva Tva = new Tva("reduite",12.0);
		daosTva.getTVADAO().create(Tva);
		assertEquals(size + 1 , daosTva.getTVADAO().findAllTva().size());
		daosTva.getTVADAO().delete(Tva);

	}

	@Test
	public void testDeleteTva()
	{
		DAOFactory daosTva = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		Tva Tva = new Tva("normale",20.2);
		int id = daosTva.getTVADAO().create(Tva);
		Tva.setIdTva(id);
		int size  = daosTva.getTVADAO().findAllTva().size();
		daosTva.getTVADAO().delete(Tva);
		assertEquals(size - 1 , daosTva.getTVADAO().findAllTva().size());
		assertNotEquals(Tva,daosTva.getTVADAO().getById(id) );

	}
	
	@Test
	public void testUpdateTva()
	{
		DAOFactory daosTva = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		Tva Tva1 = daosTva.getTVADAO().getById(1);
		Tva Tva2 = new Tva("reduite",10.2);
		daosTva.getTVADAO().update(Tva2);
		assertNotEquals(Tva1,daosTva.getTVADAO().getById(1));
		daosTva.getTVADAO().update(Tva1);
	}
}
