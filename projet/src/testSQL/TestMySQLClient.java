package testSQL;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Scanner;

import org.junit.Test;

import dao.DAOFactory;
import dao.EPersistance;
import metiers.Client;
import metiers.Produit;

public class TestMySQLClient {
	


	@Test
	public void testAddClient(){
		
		DAOFactory daosClient = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		int size  = daosClient.getClientDAO().findAllClient().size();
		Client client = new Client("zingraff","rue de la mairie");
		daosClient.getClientDAO().create(client);
		assertEquals(size + 1 , daosClient.getClientDAO().findAllClient().size());
		daosClient.getClientDAO().delete(client);
		
	}
	
	@Test
	public void testDeleteClient()
	{
		DAOFactory daosClient = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		Client client = new Client("Theo","ruede la mairie");
		int id = daosClient.getClientDAO().create(client);
		client.setIdclient(id);
		int size  = daosClient.getClientDAO().findAllClient().size();
		daosClient.getClientDAO().delete(client);
		assertEquals(size - 1 , daosClient.getClientDAO().findAllClient().size());
		assertNotEquals(client,daosClient.getClientDAO().getById(id) );



	}
	
	@Test
	public void testUpdateClient()
	{
		DAOFactory daosClient = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		Client client1 = daosClient.getClientDAO().getById(1);
		Client client2 = new Client(1,"Jules", "Rue principale");
		daosClient.getClientDAO().update(client2);
		System.out.println(daosClient.getClientDAO().getById(1));
		assertNotEquals(client1,daosClient.getClientDAO().getById(1));
		daosClient.getClientDAO().update(client1);
	}
	}
