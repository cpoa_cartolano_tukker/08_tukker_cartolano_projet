package testSQL;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import org.junit.Test;

import dao.DAOFactory;
import dao.EPersistance;
import metiers.Client;
import metiers.Facture;
import metiers.Produit;

public class TestMySQLFacture {
	
	DateTimeFormatter germanFormatter = DateTimeFormatter.ofLocalizedDate(
	        FormatStyle.MEDIUM).withLocale(Locale.GERMAN);



	@Test
	public void testAddFacture(){
		
		DAOFactory daosFacture = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		int size  = daosFacture.getFactureDAO().findAllFacture().size();
		Facture facture = new Facture(2,LocalDate.parse("24.12.2014", germanFormatter));
		daosFacture.getFactureDAO().create(facture);
		assertEquals(size + 1 , daosFacture.getFactureDAO().findAllFacture().size());
		daosFacture.getFactureDAO().delete(facture);

	}
	
	@Test
	public void testDeleteFacture()
	{
		DAOFactory daosFacture = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		Facture Facture = new Facture(3,LocalDate.parse("24.12.2014", germanFormatter));
		int id = daosFacture.getFactureDAO().create(Facture);
		Facture.setIdfacture(id);
		int size  = daosFacture.getFactureDAO().findAllFacture().size();
		daosFacture.getFactureDAO().delete(Facture);
		assertEquals(size - 1 , daosFacture.getFactureDAO().findAllFacture().size());
		assertNotEquals(Facture,daosFacture.getFactureDAO().getById(id) );



	}
	
	@Test
	public void testUpdateFacture()
	{
		DAOFactory daosFacture = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		Facture Facture1 = daosFacture.getFactureDAO().getById(1);
		Facture Facture2 = new Facture(1,4,LocalDate.parse("24.12.2014", germanFormatter));
		daosFacture.getFactureDAO().update(Facture2);
		assertNotEquals(Facture1,daosFacture.getFactureDAO().getById(1));
		daosFacture.getFactureDAO().update(Facture1);
	}
}
