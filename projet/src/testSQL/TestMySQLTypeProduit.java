package testSQL;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Scanner;

import org.junit.Test;

import dao.DAOFactory;
import dao.EPersistance;
import metiers.Produit;
import metiers.TypeProduit;

public class TestMySQLTypeProduit {
	
	@Test
	public void testAddTypeProduit(){
		
		DAOFactory daosTypeProduit = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		int size  = daosTypeProduit.getTypeProduitDAO().findAllTypeProduit().size();
		TypeProduit TypeProduit = new TypeProduit("vehicule");
		daosTypeProduit.getTypeProduitDAO().create(TypeProduit);
		assertEquals(size + 1 , daosTypeProduit.getTypeProduitDAO().findAllTypeProduit().size());
	

	}
	

	@Test
	public void testDeleteTypeProduit()
	{
		DAOFactory daosTypeProduit = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		TypeProduit TypeProduit = new TypeProduit("Patisserie");
		int id = daosTypeProduit.getTypeProduitDAO().create(TypeProduit);
		TypeProduit.setIdtype(id);
		int size  = daosTypeProduit.getTypeProduitDAO().findAllTypeProduit().size();
		daosTypeProduit.getTypeProduitDAO().delete(TypeProduit);
		assertEquals(size - 1 , daosTypeProduit.getTypeProduitDAO().findAllTypeProduit().size());
		assertNotEquals(TypeProduit,daosTypeProduit.getTypeProduitDAO().getById(id) );

	}
	
	@Test
	public void testUpdateTypeProduit()
	{
		DAOFactory daosTypeProduit = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		TypeProduit TypeProduit1 = daosTypeProduit.getTypeProduitDAO().getById(1);
		TypeProduit TypeProduit2 = new TypeProduit("Vehicule");
		daosTypeProduit.getTypeProduitDAO().update(TypeProduit2);
		assertNotEquals(TypeProduit1,daosTypeProduit.getTypeProduitDAO().getById(1));
		daosTypeProduit.getTypeProduitDAO().update(TypeProduit1);
	}
}
