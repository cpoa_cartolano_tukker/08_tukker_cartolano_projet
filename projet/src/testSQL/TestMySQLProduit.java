package testSQL;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Scanner;

import org.junit.Test;

import dao.DAOFactory;
import dao.EPersistance;
import metiers.Facture;
import metiers.Produit;

public class TestMySQLProduit {
	


	@Test
	public void testAddProduit(){
		
		DAOFactory daosProduit = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		int size  = daosProduit.getProduitDAO().findAllProduit().size();
		Produit Produit = new Produit("balle",2.3,9);
		int id = daosProduit.getProduitDAO().create(Produit);
		Produit.setRef(id);
		assertEquals(size + 1 , daosProduit.getProduitDAO().findAllProduit().size());
		assertEquals(Produit,daosProduit.getProduitDAO().getById(id));
		daosProduit.getProduitDAO().delete(Produit);
	

	}
	
	@Test
	public void testDeleteProduit()
	{
		DAOFactory daosProduit = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		Produit produit = new Produit("Casquette",30.2,4);
		int id = daosProduit.getProduitDAO().create(produit);
		produit.setRef(id);
		int size  = daosProduit.getProduitDAO().findAllProduit().size();
		daosProduit.getProduitDAO().delete(produit);
		assertEquals(size - 1 , daosProduit.getProduitDAO().findAllProduit().size());
		assertNotEquals(produit,daosProduit.getProduitDAO().getById(id) );



	}
	
	@Test
	public void testUpdateProduit()
	{
		DAOFactory daosProduit = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		Produit produit1 = daosProduit.getProduitDAO().getById(1);
		Produit produit2 = new Produit(1,"Voiture",30.2,4);
		daosProduit.getProduitDAO().update(produit2);
		assertNotEquals(produit1,daosProduit.getProduitDAO().getById(1));
		daosProduit.getProduitDAO().update(produit1);
	}
}
