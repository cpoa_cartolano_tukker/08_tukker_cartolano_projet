package dao;

import MyMemory.MyMemoryDaoFactory;
import MySQL.MySQLDaoFactory;

public abstract class DAOFactory {

	public static DAOFactory getDAOFactory(EPersistance cible) {

		DAOFactory daoF = null;

		switch (cible) {
		case MYSQL:
			// jdbc :
			daoF = new MySQLDaoFactory();
			break;
		case MEMORY:
			// Memoire :
			daoF = new MyMemoryDaoFactory();
			break;
		}
		return daoF;
	}

	public abstract InterfaceGenericTvaDao getTVADAO();

	public abstract InterfaceGenericProduitDao getProduitDAO() ;
	
	public abstract InterfaceGenericClientDao getClientDAO();
	
	public abstract InterfaceGenericFactureDao getFactureDAO();
	
	public abstract InterfaceGenericTypeProduitDao getTypeProduitDAO();


	
}
