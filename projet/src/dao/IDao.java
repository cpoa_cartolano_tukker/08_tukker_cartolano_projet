package dao;



public interface IDao<T> {
	public abstract T getById(int id);

	public abstract int  create(T objet);

	public abstract void update(T objet);

	public abstract void delete(T objet);

	
}
