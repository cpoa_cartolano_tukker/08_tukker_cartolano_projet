package dao;

import java.util.List;

import metiers.Tva;

public interface InterfaceGenericTvaDao extends IDao<Tva>{

	List<Tva> findAllTva(); 
	
}
