package dao;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connexion {

	public static Connection creerConnexion() {

		Connection maConnexion = null;

		try {
			maConnexion = DriverManager.getConnection("jdbc:mysql://infodb.iutmetz.univ-lorraine.fr/cartolan2u_projetCPOA","cartolano_appli","31504871");
		} catch (SQLException se) {
			System.out.println("Erreur de connexion :" + se.getMessage());
		}

		return maConnexion;
	}

}
